# HW2 Simple Key-value Store

## Directories
- /server ->	server program related sources
- /client ->	client program related sources
- /common ->	common inclusions
- /util ->	common utilization
- /build ->	target build directory

## Building the Project
Code out your `/server/server.c` and `/client/client.c`, then
```shell
$ make
```
Test your `/build/server` and `build/client`.

## Implementations
### Please briefly describe your multi-threading design pattern
Each client is handled by a thread and the main thread is responsible for establishing TCP connections with the clients. When a connection is established, the sockfd of the connection will be passed to the newly created thread. That is, if there are N clients connecting to the server, there will be N+1 threads on the server in total.
### Please briefly describe your data structure implementation
No self-implemented data structures used. Each KV-pair is stored on a file, where the filename is the key and the content of the file is the value.
## References
* [POSIX thread man pages](https://man7.org/linux/man-pages/man7/pthreads.7.html)
* [socket man pages](https://linux.die.net/man/7/socket)


