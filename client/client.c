#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <ctype.h>
#include <stdlib.h>

#include "sock.h"

int main(int argc, char **argv)
{
    int opt, status = 0, strnum, pid = getpid();
    size_t buflen = BUFLEN;
    char *server_host_name = NULL, *server_port = NULL;
    char *input = malloc(BUFLEN), cmd[BUFLEN], key[BUFLEN], val[BUFLEN], ret[BUFLEN], red[BUFLEN];
    /* Parsing args */
    while ((opt = getopt(argc, argv, "h:p:")) != -1)
    {
        switch (opt)
        {
        case 'h':
            server_host_name = malloc(strlen(optarg) + 1);
            strncpy(server_host_name, optarg, strlen(optarg));
            break;
        case 'p':
            server_port = malloc(strlen(optarg) + 1);
            strncpy(server_port, optarg, strlen(optarg));
            break;
        case '?':
            fprintf(stderr, "Unknown option \"-%c\"\n", isprint(optopt) ?
                    optopt : '#');
            free(input);
            return 0;
        }
    }

    if (!server_host_name)
    {
        fprintf(stderr, "Error!, No host name provided!\n");
        exit(1);
    }

    if (!server_port)
    {
        fprintf(stderr, "Error!, No port number provided!\n");
        exit(1);
    }

    /* Open a client socket fd */
    int clientfd __attribute__((unused)) = open_clientfd(server_host_name, server_port);

    while(status != -1)
    {
        bzero(key, BUFLEN);
        bzero(cmd, BUFLEN);
        bzero(val, BUFLEN);
        bzero(ret, BUFLEN);
        bzero(red, BUFLEN);
        bzero(input, BUFLEN);
        printf("> ");
        status = getline(&input, &buflen, stdin);

        strnum = sscanf(input, "%s%s%s%s", cmd, key, val, red);
        if(strnum > 3)
        {
            printf("[ERROR] Wrong command format\n");
            continue;
        }

        if(!strcmp(cmd, "SET"))
        {
            if(strnum != 3)
            {
                printf("[ERROR] Wrong command format\n");
                continue;
            }

            if(try_send(clientfd, __LINE__, cmd) < 0)
                break;
            if(try_send(clientfd, __LINE__, key) < 0)
                break;
            if(try_send(clientfd, __LINE__, val) < 0)
                break;

            if(try_recv(clientfd, __LINE__, ret) < 0)
                break;

            //printf("PID %d: ", pid);
            printf("%s", ret);
        }
        else if(!strcmp(cmd, "GET"))
        {
            //printf("executing GET\n");

            if(strnum != 2)
            {
                printf("[ERROR] Wrong command format\n");
                continue;
            }

            if(try_send(clientfd, __LINE__, cmd) < 0)
                break;
            if(try_send(clientfd, __LINE__, key) < 0)
                break;

            if(try_recv(clientfd, __LINE__, ret) < 0)
                break;

            //printf("PID %d: ", pid);
            printf("%s", ret);
        }
        else if(!strcmp(cmd, "DELETE"))
        {
            if(strnum != 2)
            {
                printf("[ERROR] Wrong command format\n");
                continue;
            }

            if(try_send(clientfd, __LINE__, cmd) < 0)
                break;
            if(try_send(clientfd, __LINE__, key) < 0)
                break;

            if(try_recv(clientfd, __LINE__, ret) < 0)
                break;

            //printf("PID %d: ", pid);
            printf("%s", ret);
        }
        else if(!strcmp(cmd, "EXIT"))
        {
            close(clientfd);
            break;
        }
        else
        {
            printf("[ERROR] unknown command \"%s\"\n", cmd);
            continue;
        }
    }

    free(input);
    free(server_port);
    free(server_host_name);

    return 0;
}
