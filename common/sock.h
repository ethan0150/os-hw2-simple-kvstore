#ifndef _SOCK_H_
#define _SOCK_H_

#define BUFLEN 1024

int try_send(int, int, char *);
int try_recv(int, int, char *);
int open_clientfd(char *hostname, char *port) __attribute__((unused));
int open_listenfd(char *port) __attribute__((unused));

#endif

