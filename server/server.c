#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <netdb.h>
#include <ctype.h>
#include <stdatomic.h>

#define FREE_RESRC close(listenfd);free(server_port);
#define KEY_NF "[ERROR] Specified key not found!\n"
#define F_TYPE ".rec"

#include "types.h"
#include "sock.h"

FILE *fp;
static pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;

void* cli_handler(void *arg)
{
    int sockfd = *((int *)arg);
    free(arg);
    char ret[BUFLEN], cmd[BUFLEN], key[BUFLEN], filename[BUFLEN], val[BUFLEN];
    bzero(cmd, BUFLEN);

    pthread_t tid = pthread_self();

    while(recv(sockfd, cmd, BUFLEN, 0) > 0) // recv db command from client
    {
        bzero(key, BUFLEN);
        bzero(ret, BUFLEN);
        bzero(filename, BUFLEN);
        bzero(val, BUFLEN);

        if (try_recv(sockfd, __LINE__, key) < 0) // recv key
            return NULL;

        strcpy(filename, key);
        strcat(filename, F_TYPE);

        if(!strcmp(cmd, "GET"))
        {
            pthread_mutex_lock(&mtx);
            //sleep(3);
            if(access(filename, F_OK) != 0)  // file not exist
            {
                pthread_mutex_unlock(&mtx);

                sprintf(ret, "[ERROR] Specified key \"%s\" not found!\n", key);
                if(try_send(sockfd, __LINE__, ret) < 0)
                    return NULL;

                continue;
            }

            fp = fopen(filename, "r");
            fscanf(fp, "%s", val);
            fclose(fp);
            pthread_mutex_unlock(&mtx);

            sprintf(ret, "[OK] The value of %s is %s\n", key, val);
            if(try_send(sockfd, __LINE__, ret) < 0)
                return NULL;
            printf("[OK] Thread %ld completed command \"%s %s\"\n", tid, cmd, key);
        }
        if(!strcmp(cmd, "SET"))
        {
            if(try_recv(sockfd, __LINE__, val) < 0)
                return NULL;

            pthread_mutex_lock(&mtx);
            //sleep(3);
            if(access(filename, F_OK) == 0)  // file exists
            {
                pthread_mutex_unlock(&mtx);

                sprintf(ret, "[ERROR] Specified key \"%s\" already exists\n", key);
                if(try_send(sockfd, __LINE__, ret) < 0)
                    return NULL;

                continue;
            }

            fp = fopen(filename, "w");
            fprintf(fp, "%s", val);
            fclose(fp);
            pthread_mutex_unlock(&mtx);

            sprintf(ret, "[OK] Key-value pair (%s, %s) is set\n", key, val);
            if(try_send(sockfd, __LINE__, ret) < 0)
                return NULL;

            printf("[OK] Thread %ld completed command \"%s %s %s\"\n", tid, cmd, key, val);
        }
        if(!strcmp(cmd, "DELETE"))
        {
            pthread_mutex_lock(&mtx);
            //sleep(3);
            if(access(filename, F_OK) != 0)  // file not exist
            {
                pthread_mutex_unlock(&mtx);

                sprintf(ret, "[ERROR] Specified key \"%s\" not found!\n", key);
                if(try_send(sockfd, __LINE__, ret) < 0)
                    return NULL;

                continue;
            }

            remove(filename);
            pthread_mutex_unlock(&mtx);

            sprintf(ret, "[OK] Key \"%s\" is deleted\n", key);
            if(try_send(sockfd, __LINE__, ret) < 0)
                return NULL;
            printf("[OK] Thread %ld completed command \"%s %s\"\n", tid, cmd, key);
        }
        if(!strcmp(cmd, "EXIT"))
        {
            break;
        }
        bzero(cmd, BUFLEN);
    }

    printf("exiting thread\n");
    close(sockfd);
    return NULL;
}

int main(int argc, char **argv)
{
    char *server_port = 0;
    int opt = 0;

    int *pasv_sockfd;
    struct sockaddr_in cli_addr;
    socklen_t cli_addrlen = sizeof(struct sockaddr_in);

    pthread_t tid;
    pthread_attr_t attr;
    pthread_attr_init(&attr);

    /* Parsing args */
    while ((opt = getopt(argc, argv, "p:")) != -1)
    {
        switch (opt)
        {
        case 'p':
            server_port = malloc(strlen(optarg) + 1);
            strncpy(server_port, optarg, strlen(optarg));
            break;
        case '?':
            fprintf(stderr, "Unknown option \"-%c\"\n", isprint(optopt) ?
                    optopt : '#');
            return 0;
        }
    }

    if (!server_port)
    {
        fprintf(stderr, "Error! No port number provided!\n");
        exit(1);
    }

    /* Open a listen socket fd */
    int listenfd __attribute__((unused)) = open_listenfd(server_port);
    // Make threads detached by default
    if(pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED) != 0)
    {
        perror("pthread_attr_setdetachstate()");
        close(listenfd);
        return -1;
    }

    system("rm ./*.rec");

    while(1)
    {
        pasv_sockfd = malloc(sizeof(int));
        *pasv_sockfd = accept(listenfd, (struct sockaddr*)&cli_addr, &cli_addrlen);

        if (*pasv_sockfd == -1)
        {
            perror("accept()");
            FREE_RESRC
            return -1;
        }

        if(pthread_create(&tid, &attr, &cli_handler, pasv_sockfd) > 0)
        {
            perror("pthread_create()");
            FREE_RESRC
            return -1;
        }
    }
    return 0;
}

